import os
from datetime import date, timedelta, datetime
import configparser
import garminconnect
import logging

# Initialize logger
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Reading config file
config = configparser.ConfigParser()
config.read('.app-data/.config/config.ini')
email = config['GarminLogin']['email']
password = config['GarminLogin']['password']
date_from =  config['GarminDownload']['date_from']
date_from = datetime.strptime(date_from, "%Y-%m-%d").date()

# Garmin setup/connection
garmin = garminconnect.Garmin(email, password)
garmin.login()
GARTH_HOME = os.getenv("GARTH_HOME", "~/.garth")
garmin.garth.dump(GARTH_HOME)

# Define columns
columns = ['userProfileId','totalKilocalories','activeKilocalories','bmrKilocalories','wellnessKilocalories','burnedKilocalories','consumedKilocalories','remainingKilocalories','totalSteps','netCalorieGoal','totalDistanceMeters','wellnessDistanceMeters','wellnessActiveKilocalories','netRemainingKilocalories','userDailySummaryId','calendarDate','uuid','dailyStepGoal','wellnessStartTimeGmt','wellnessStartTimeLocal','wellnessEndTimeGmt','wellnessEndTimeLocal','durationInMilliseconds','wellnessDescription','highlyActiveSeconds','activeSeconds','sedentarySeconds','sleepingSeconds','includesWellnessData','includesActivityData','includesCalorieConsumedData','privacyProtected','moderateIntensityMinutes','vigorousIntensityMinutes','floorsAscendedInMeters','floorsDescendedInMeters','floorsAscended','floorsDescended','intensityMinutesGoal','userFloorsAscendedGoal','minHeartRate','maxHeartRate','restingHeartRate','lastSevenDaysAvgRestingHeartRate','source','averageStressLevel','maxStressLevel','stressDuration','restStressDuration','activityStressDuration','uncategorizedStressDuration','totalStressDuration','lowStressDuration','mediumStressDuration','highStressDuration','stressPercentage','restStressPercentage','activityStressPercentage','uncategorizedStressPercentage','lowStressPercentage','mediumStressPercentage','highStressPercentage','stressQualifier','measurableAwakeDuration','measurableAsleepDuration','lastSyncTimestampGMT','minAvgHeartRate','maxAvgHeartRate','bodyBatteryChargedValue','bodyBatteryDrainedValue','bodyBatteryHighestValue','bodyBatteryLowestValue','bodyBatteryMostRecentValue','bodyBatteryVersion','abnormalHeartRateAlertsCount','averageSpo2','lowestSpo2','latestSpo2','latestSpo2ReadingTimeGmt','latestSpo2ReadingTimeLocal','averageMonitoringEnvironmentAltitude','restingCaloriesFromActivity','avgWakingRespirationValue','highestRespirationValue','lowestRespirationValue','latestRespirationValue','latestRespirationTimeGMT']

# Add headers if no file exists
if not os.path.isfile (".app-data/.data/garmin_stats_v1.csv"):
    header = ""
    for col in columns:
        header = header + col + ","
    with open(".app-data/.data/garmin_stats_v1.csv", 'a') as outfile:
        outfile.write(header + "\n")

# Download data
while date_from < date.today():
    data = garmin.get_stats(date_from.isoformat())
    with open(".app-data/.data/garmin_stats_v1.csv", 'a') as outfile:
        formatted_data = ""
        for key in columns:
            if key == 'rule':
                continue

            value = data.get(key)
            if value is None:
                formatted_data = formatted_data + "" + ","
            else:
                formatted_data = formatted_data + str(value) + ","
                
        outfile.write(formatted_data + "\n")

    date_from = date_from + timedelta(days=1)

    # config['GarminDownload']['date_from'] = date_from

# Update config with new date_from
# with open('.app-data/.config/config.ini', 'w') as configfile:    # TODO: incremental updates
#     config.write(configfile)
        